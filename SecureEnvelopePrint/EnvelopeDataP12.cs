﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureEnvelopePrintNS
{
    public class EnvelopeDataP12 : EnvelopeData
    {
        public string RequestNumber { get; set; }
        public string AuthorizationCode { get; set; }

       
        public EnvelopeDataP12(string printerName, EnvelopeTypeEnum envelopeType, string cn, string requestNumber, string authorizationCode, string legalEntityName, string name, string lastName, string street, string streetNo, string postNo, string city, string pak, int pageWidth, int pageHeight)
            : base(printerName, envelopeType, cn, legalEntityName, name, lastName, street, streetNo, postNo, city, pak, pageWidth, pageHeight)
        {
            RequestNumber = requestNumber;
            AuthorizationCode = authorizationCode;
        }
    }
}
