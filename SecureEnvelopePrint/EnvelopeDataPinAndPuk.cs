﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureEnvelopePrintNS
{
    public class EnvelopeDataPinAndPuk : EnvelopeData
    {
        public string Pin { get; set; }
        public string Puk { get; set; }
        
        public EnvelopeDataPinAndPuk(string printerName, EnvelopeTypeEnum envelopeType, string pin, string puk, string cn, string legalEntityName, string name, string lastName, string street, string streetNo, string postNo, string city, string pak, int pageWidth, int pageHeight)
            : base(printerName, envelopeType, cn, legalEntityName, name, lastName, street, streetNo, postNo, city, pak, pageWidth, pageHeight)
        {
            Pin = pin;
            Puk = puk;
            LegalEntityName = legalEntityName;
            Name = name;
            LastName = lastName;
            Street = street;
            StreetNo = streetNo;
            PostNo = postNo;
            City = city;
            Pak = pak;
        }
    }
}
