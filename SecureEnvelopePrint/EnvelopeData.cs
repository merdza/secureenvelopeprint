﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureEnvelopePrintNS
{
    public abstract class EnvelopeData
    {
        public string PrinterName { get; set; }
        public EnvelopeTypeEnum EnvelopeType { get; set; }
        public string DateOfPrinting { get; set; }
        public string CommonName { get; set; }
        public string LegalEntityName { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string PostNo { get; set; }
        public string City { get; set; }
        public string Pak { get; set; }
        public int PageWidth { get; set; }  // u pikselima. 1 inch = 96 px
        public int PageHeight { get; set; }  // u pikselima. 1 inch = 96 px

        public EnvelopeData(string printerName, EnvelopeTypeEnum envelopeType, string cn, string legalEntityName, string name, string lastName, string street, string streetNo, string postNo, string city, string pak, int pageWidth, int pageHeight)
        {
            PrinterName = printerName;
            EnvelopeType = envelopeType;
            DateOfPrinting = Utils.GetShortSerbianDate(DateTime.Now, true);
            CommonName = cn;
            LegalEntityName = legalEntityName;
            Name = name;
            LastName = lastName;
            Street = street;
            StreetNo = streetNo;
            PostNo = postNo;
            City = city;
            Pak = pak;
            PageWidth = pageWidth;
            PageHeight = pageHeight;
        }

        
    }


}
