﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SecureEnvelopePrintNS
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        

        void App_Startup(object sender, StartupEventArgs e)
        {
            // Application is running
            // Process command line args

            MainWindow mainWindow = new MainWindow(e.Args);
           
        }


        
    }

    public enum EnvelopeTypeEnum { PIN, PUK, PINPUK, p12, p12Password }
}
