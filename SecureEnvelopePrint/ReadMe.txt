﻿SecureEnvelopePrint
-------------------

Small desktop application for printing reports from XAML templates. Its primary purpose is to print secret data reports on closed envelopes, such as PIN, PUK and password.

XAML templates are part of the exe file and can be changed only in the project.

Only SecureEnvelopePrint.exe file is needed to execute program. Parameters to be printed should be provided on the Standard Input Stream. Parameters should be provided in the format parameterName1=parameterValue1;parameterName2=parameterValue2;... Key case is sensitive. Parameters are UTF-8 encoded. Semicolon after last parameter is not mandatory.
If parameters supposed to be provided by some other application, a newline character must be provided at the end of parameter values.

Required parameters:
printer (name of the printer on which the report should be printed)
MessageType (possible values: PIN, PUK, PINPUK, p12, p12Password)

Optional parameters:
PIN (value of the PIN)
PUK (value of the PUK)
CN (value of the Common Name of the certificate)
legalEntityName (Company name)
givenName (first name)
lastName
street
houseNumber
postalCode
city
PAK (value of the address PAK)
pageWidth (width of the page to be printed in pixels. 1in = 96px. if not provided, default value is 768 which is equal to 8 inches)
pageHeight (height of the page to be printed in pixels. 1in = 96px. if not provided, default value is 384 which is equal to 4 inches)

Parameters example:
printer=PDF Printer;MessageType=PIN;PIN=2375;CN=CN value;legalEntityName=Some company;givenName=John;lastName=Smith;street=First boulevard;houseNumber=111;postalCode=11000;city=Beograd;PAK=123456;pageWidth=595;pageHeight=842;

