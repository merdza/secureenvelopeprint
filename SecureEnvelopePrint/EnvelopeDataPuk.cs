﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureEnvelopePrintNS
{
    public class EnvelopeDataPuk : EnvelopeDataPinAndPuk
    {
        //puk je prazan string
        public EnvelopeDataPuk(string printerName, EnvelopeTypeEnum envelopeType, string puk, string cn, string legalEntityName, string name, string lastName, string street, string streetNo, string postNo, string city, string pak, int pageWidth, int pageHeight)
            : base(printerName, envelopeType, string.Empty, puk, cn, legalEntityName, name, lastName, street, streetNo, postNo, city, pak, pageWidth, pageHeight)
        {
        }
    }
}
