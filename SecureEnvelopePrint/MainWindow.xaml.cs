﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;

namespace SecureEnvelopePrintNS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public EnvelopeData MyEnvelopeData { get; set; }
        protected Dictionary<string, string> Arguments;

        public MainWindow(string[] args)
        {
            /*
             1.	MessageType=PIN:
SecureEnvelopePrint printer=PDF Printer;MessageType=PIN;PIN=2375;CN=CN value;legalEntityName=Some company;givenName=Miloš;lastName=Merdžanović;street=Simina;houseNumber=1;postalCode=11000;city=Beograd;PAK=123456
2.	MessageType=PUK:
SecureEnvelopePrint printer=PDF Printer;MessageType=PUK;PUK=87533578;CN=CN value;legalEntityName=Some company;givenName=Miloš;lastName=Merdžanović;street=Simina;houseNumber=1;postalCode=11000;city=Beograd;PAK=123456
3.	MessageType=PINPUK:
SecureEnvelopePrint printer=PDF Printer;MessageType=PINPUK;PIN=2375;PUK=87533578;CN=CN value;legalEntityName=Some company;givenName=Miloš;lastName=Merdžanović;street=Simina;houseNumber=1;postalCode=11000;city=Beograd;PAK=123456
4.	MessageType=p12:
SecureEnvelopePrint printer=PDF Printer;MessageType=p12;requestNumber=20000001;downloadAuthorizationCode=someValue;legalEntityName=Some company;givenName=Miloš;lastName=Merdžanović;street=Simina;houseNumber=1;postalCode=11000;city=Beograd;PAK=123456
5.	MessageType=p12Password:
SecureEnvelopePrint printer=PDF Printer;MessageType=p12Password;password=someValue;legalEntityName=Some company;givenName=Miloš;lastName=Merdžanović;street=Simina;houseNumber=1;postalCode=11000;city=Beograd;PAK=123456
             */

            //DEBUG ONLY
            //args = new string[] { @"printer=PDF Printer;MessageType=PIN;PIN=1234;CN=Željko Joksimović 200000062;legalEntityName=test;givenName=Željko;lastName=Joksimović;street=Studentski park;houseNumber=11;postalCode=12345;city=Beograd;PAK=6789;\r" };


            try
            {
                InitializeComponent();
                InitializeData(/*args*/);  //argumenti se vise ne salju na ovaj nacin, nego preko standard inputa - vidi objasnjenje u funkciji
                DataContext = MyEnvelopeData;
                ProcessDocument();

                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                // Write e.message to standard error stream for the BlueX

                // Convert message to utf-8      
                Encoding utf8Encoding = new UTF8Encoding(false);    // UTF-8 transcoder without BOM
                byte[] utf8Message = utf8Encoding.GetBytes(ex.Message);

                // Write message to standard error stream
                foreach (byte b in utf8Message)
                    Console.Error.Write((char)b);

                //Environment.ExitCode = 1;        // Set process exit code to 1

                //todo za sada se greska ispisuje i u Event Log, ali ako BlueX moze da hendluje gresku ispisanu gore, onda je ovo nepotrebno
                Utils.WriteToEventLog(String.Format("Secure envelope print error: {0}", ex.Message), EventLogEntryType.Error);
                Environment.Exit(1);
            }
        }

        public void InitializeData(/*string[] args*/)
        {
            try
            {
                Encoding utf8Encoding = new UTF8Encoding(false);    // UTF-8 transcoder without BOM

                // Open standard input stream and get a stream reader that can handle the conversion from utf-8 for us
                Stream stdIn = Console.OpenStandardInput();
                StreamReader stdInReader = new StreamReader(stdIn, utf8Encoding);

                // Open standard output stream and get a stream writer that can handle the conversion to utf-8 for us
                Stream stdOut = Console.OpenStandardOutput();
                StreamWriter stdOutWriter = new StreamWriter(stdOut, utf8Encoding)
                {
                    AutoFlush = true
                };

                // Read input from stream until the first newline character
                String arguments = stdInReader.ReadLine();

                //*********************************************************************************
                // ovako kao sto je ispod je radilo dok smo mislili da parametri mogu da se proslede kao da se program izvrsava iz komandne linije
                // npr. SecureEnvelopePrint printer=PDF Printer;MessageType=PIN;PIN=2375;CN=CN value;legalEntityName=Some company;givenName=Miloš;lastName=Merdžanović;street=Simina;houseNumber=1;postalCode=11000;city=Beograd;PAK=123456
                // ali ne moze tako, nego parametri, zbog BlueX-a moraju da se dobiju sa standardnog inputa, odnosno kao da ih korisnik unosi rucno
                // zato postoji gornji kod, a ne ovaj koji je iskomentarisan

                //string arguments = string.Empty;
                //sve treba da bude jedan argument, bez obzira na broj spejsova - jer su u komandnoj liniji argumenti odvojeni spejsovima, a nama je to problem ako npr. ime printera ima spejs

                //foreach (var arg in args)
                //{
                //    if (arguments.Equals(string.Empty))
                //    {
                //        arguments = arg;
                //    }
                //    else
                //    {
                //        arguments += " " + arg;
                //    }
                //}
                //*********************************************************************************
                
                //Utils.WriteToEventLog(String.Format("Parameters: {0}", arguments), EventLogEntryType.Information);

                ParseArguments(arguments);
            }
            catch (Exception e)
            {
                throw new Exception("Parameter reading from StdIn error. " + e.Message);
            }
        }

        private void ParseArguments(string args)
        {
            Arguments = new Dictionary<string, string>();
            
            string[] parameters = args.Split(';');

            foreach (string parameter in parameters)
            {
                string[] nameValue = parameter.Split('=');
                try
                {
                    Arguments.Add(nameValue[0], nameValue[1]);
                }
                catch (Exception)
                {
                    //ako ne postoji par x=y, onda to nije ispravan parametar, pa ignorisi to
                }
            }

            string envelopeType = GetParameter("MessageType");
            if (envelopeType.Equals(string.Empty))
            {
                throw new Exception("MessageType parameter was not provided and it is mandatory");
            }
            EnvelopeTypeEnum envelopeTypeEnum = (EnvelopeTypeEnum)System.Enum.Parse(typeof(EnvelopeTypeEnum), envelopeType);

            MyEnvelopeData = GetEnvelopeData(envelopeTypeEnum);

            //Utils.WriteToEventLog(String.Format("Argumenti: {0}, {1}", printerName, pinValue));

        }

        private EnvelopeData GetEnvelopeData(EnvelopeTypeEnum envelopeType)
        {
            EnvelopeData envelopeData = null;

            int pageWidth;
            int pageHeight;

            try
            {
                pageWidth = Convert.ToInt32(GetParameter("pageWidth"));
            }
            catch (Exception)
            {
                pageWidth = 768;  //default vrednost u px. 1in=96px, ovde je 8in
            }

            try
            {
                pageHeight = Convert.ToInt32(GetParameter("pageHeight"));
            }
            catch (Exception)
            {
                pageHeight = 384;  //default vrednost u px. 1in=96px, ovde je 4in
            }

            switch (envelopeType)
            {
                case EnvelopeTypeEnum.PIN:
                    envelopeData = new EnvelopeDataPin(GetParameter("printer"),
                                                       envelopeType,
                                                       GetParameter("PIN"),
                                                       GetParameter("CN"),
                                                       GetParameter("legalEntityName"),
                                                       GetParameter("givenName"),
                                                       GetParameter("lastName"),
                                                       GetParameter("street"),
                                                       GetParameter("houseNumber"),
                                                       GetParameter("postalCode"),
                                                       GetParameter("city"),
                                                       GetParameter("PAK"),
                                                       pageWidth,
                                                       pageHeight
                        );
                    break;
                case EnvelopeTypeEnum.PUK:
                    envelopeData = new EnvelopeDataPuk(GetParameter("printer"),
                                                       envelopeType,
                                                       GetParameter("PUK"),
                                                       GetParameter("CN"),
                                                       GetParameter("legalEntityName"),
                                                       GetParameter("givenName"),
                                                       GetParameter("lastName"),
                                                       GetParameter("street"),
                                                       GetParameter("houseNumber"),
                                                       GetParameter("postalCode"),
                                                       GetParameter("city"),
                                                       GetParameter("PAK"),
                                                       pageWidth,
                                                       pageHeight
                        );
                    break;
                case EnvelopeTypeEnum.PINPUK:
                    envelopeData = new EnvelopeDataPinAndPuk(GetParameter("printer"),
                                                       envelopeType,
                                                       GetParameter("PIN"),
                                                       GetParameter("PUK"),
                                                       GetParameter("CN"),
                                                       GetParameter("legalEntityName"),
                                                       GetParameter("givenName"),
                                                       GetParameter("lastName"),
                                                       GetParameter("street"),
                                                       GetParameter("houseNumber"),
                                                       GetParameter("postalCode"),
                                                       GetParameter("city"),
                                                       GetParameter("PAK"),
                                                       pageWidth,
                                                       pageHeight
                        );
                    break;
                case EnvelopeTypeEnum.p12:
                    envelopeData = new EnvelopeDataP12(GetParameter("printer"),
                                                       envelopeType,
                                                       GetParameter("CN"),
                                                       GetParameter("requestNumber"),
                                                       GetParameter("downloadAuthorizationCode"),
                                                       GetParameter("legalEntityName"),
                                                       GetParameter("givenName"),
                                                       GetParameter("lastName"),
                                                       GetParameter("street"),
                                                       GetParameter("houseNumber"),
                                                       GetParameter("postalCode"),
                                                       GetParameter("city"),
                                                       GetParameter("PAK"),
                                                       pageWidth,
                                                       pageHeight
                        );
                    break;
                case EnvelopeTypeEnum.p12Password:
                    envelopeData = new EnvelopeDataP12Password(GetParameter("printer"),
                                                       envelopeType,
                                                       GetParameter("CN"),
                                                       GetParameter("password"),
                                                       GetParameter("legalEntityName"),
                                                       GetParameter("givenName"),
                                                       GetParameter("lastName"),
                                                       GetParameter("street"),
                                                       GetParameter("houseNumber"),
                                                       GetParameter("postalCode"),
                                                       GetParameter("city"),
                                                       GetParameter("PAK"),
                                                       pageWidth,
                                                       pageHeight
                        );
                    break;
                default:
                    break;
            }

            return envelopeData;
        }

        private string GetParameter(string parameterName)
        {
            string parameterValue = string.Empty;
            if (Arguments.TryGetValue(parameterName, out parameterValue))
            {
                return parameterValue;
            }
            else
            {
                return string.Empty;
            }
        }

        

        private void ProcessDocument()
        {
            try
            {
                //dodeli odgovarajuci sablon za stampu
                FlowDocument document = null;
                switch (MyEnvelopeData.EnvelopeType)
                {
                    case EnvelopeTypeEnum.PIN:
                        document = XamlReader.Parse(SecureEnvelopePrintNS.Properties.Resources.PinTemplate) as FlowDocument;
                        break;
                    case EnvelopeTypeEnum.PUK:
                        document = XamlReader.Parse(SecureEnvelopePrintNS.Properties.Resources.PukTemplate) as FlowDocument;
                        break;
                    case EnvelopeTypeEnum.PINPUK:
                        document = XamlReader.Parse(SecureEnvelopePrintNS.Properties.Resources.PinPukTemplate) as FlowDocument;
                        break;
                    case EnvelopeTypeEnum.p12:
                        document = XamlReader.Parse(SecureEnvelopePrintNS.Properties.Resources.P12Template) as FlowDocument;
                        break;
                    case EnvelopeTypeEnum.p12Password:
                        document = XamlReader.Parse(SecureEnvelopePrintNS.Properties.Resources.P12PasswordTemplate) as FlowDocument;
                        break;
                    default:
                        break;
                }
                
                //ubaci parametre u dokument
                InjectData(document, MyEnvelopeData);

                // ako treba da se izlistaju svi printeri na sistemu, za to sluzi ova metoda ispod
                //ListAllPrinters();

                //stampaj dokument
                PrintDocument(document);

                //Utils.WriteToEventLog("Uspesno sve zavrseno");

            }
            catch (Exception ex)
            {
                throw new Exception("Error in processing document for printing. " + ex.Message);
            }
        }

        protected void InjectData(FlowDocument document, EnvelopeData envelopeData)
        {
            switch (envelopeData.EnvelopeType)
            {
                case EnvelopeTypeEnum.PIN:
                    EnvelopeDataPin dtPin = (EnvelopeDataPin)envelopeData;
                    document.DataContext = new
                    {
                        dtPin.PrinterName,
                        dtPin.Pin,
                        dtPin.CommonName,
                        dtPin.LegalEntityName,
                        dtPin.Name,
                        dtPin.LastName,
                        dtPin.Street,
                        dtPin.StreetNo,
                        dtPin.PostNo,
                        dtPin.City,
                        dtPin.Pak,
                        dtPin.DateOfPrinting
                    };
                    break;
                case EnvelopeTypeEnum.PUK:
                    EnvelopeDataPuk dtPuk = (EnvelopeDataPuk)envelopeData;
                    document.DataContext = new
                    {
                        dtPuk.PrinterName,
                        dtPuk.Puk,
                        dtPuk.CommonName,
                        dtPuk.LegalEntityName,
                        dtPuk.Name,
                        dtPuk.LastName,
                        dtPuk.Street,
                        dtPuk.StreetNo,
                        dtPuk.PostNo,
                        dtPuk.City,
                        dtPuk.Pak,
                        dtPuk.DateOfPrinting
                    };
                    break;
                case EnvelopeTypeEnum.PINPUK:
                    EnvelopeDataPinAndPuk dtPinPuk = (EnvelopeDataPinAndPuk)envelopeData;
                    document.DataContext = new
                    {
                        dtPinPuk.PrinterName,
                        dtPinPuk.Pin,
                        dtPinPuk.Puk,
                        dtPinPuk.CommonName,
                        dtPinPuk.LegalEntityName,
                        dtPinPuk.Name,
                        dtPinPuk.LastName,
                        dtPinPuk.Street,
                        dtPinPuk.StreetNo,
                        dtPinPuk.PostNo,
                        dtPinPuk.City,
                        dtPinPuk.Pak,
                        dtPinPuk.DateOfPrinting
                    };
                    break;
                case EnvelopeTypeEnum.p12:
                    EnvelopeDataP12 p12 = (EnvelopeDataP12)envelopeData;
                    document.DataContext = new
                    {
                        p12.PrinterName,
                        p12.RequestNumber,
                        p12.AuthorizationCode,
                        p12.CommonName,
                        p12.LegalEntityName,
                        p12.Name,
                        p12.LastName,
                        p12.Street,
                        p12.StreetNo,
                        p12.PostNo,
                        p12.City,
                        p12.Pak,
                        p12.DateOfPrinting
                    };
                    break;
                case EnvelopeTypeEnum.p12Password:
                    EnvelopeDataP12Password dtP12Password = (EnvelopeDataP12Password)envelopeData;
                    document.DataContext = new
                    {
                        dtP12Password.PrinterName,
                        dtP12Password.Password,
                        dtP12Password.CommonName,
                        dtP12Password.LegalEntityName,
                        dtP12Password.Name,
                        dtP12Password.LastName,
                        dtP12Password.Street,
                        dtP12Password.StreetNo,
                        dtP12Password.PostNo,
                        dtP12Password.City,
                        dtP12Password.Pak,
                        dtP12Password.DateOfPrinting
                    };
                    break;
                default:
                    break;
            }                

            // we need to give the binding infrastructure a push as we
            // are operating outside of the intended use of WPF
            var dispatcher = Dispatcher.CurrentDispatcher;
            dispatcher.Invoke(
                DispatcherPriority.SystemIdle,
                new DispatcherOperationCallback(delegate { return null; }),
                null);
        }

        protected void ListAllPrinters()
        {
            LocalPrintServer printServer = new LocalPrintServer();

            PrintQueueCollection printQueuesOnLocalServer = printServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });

            string printers = string.Empty;
            foreach (PrintQueue printer in printQueuesOnLocalServer)
            {
                printers += printer.Name + ", ";
            }
            Utils.WriteToEventLog(printers, EventLogEntryType.Information);
        }

        protected void PrintDocument(FlowDocument document)
        {
            PrintDialog printDialog = new PrintDialog();

            LocalPrintServer printServer = new LocalPrintServer();
            PrintQueueCollection printQueuesOnLocalServer = printServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });


            bool succ = false;
            foreach (PrintQueue printer in printQueuesOnLocalServer)
            {
                if (printer.Name.Equals(MyEnvelopeData.PrinterName))
                {
                    printDialog.PrintQueue = printer;
                    succ = true;
                    break;
                    //Utils.WriteToEventLog("Odabran je printer: " + printer.Name, EventLogEntryType.Information);
                }
            }

            if (succ)
            {
                //Printdlg.PrintQueue = new PrintQueue(new PrintServer(), ((App)Application.Current).MyData.PrinterName);
                printDialog.PrintTicket.PageOrientation = System.Printing.PageOrientation.Portrait;
                printDialog.PrintTicket.PageMediaSize = new PageMediaSize(MyEnvelopeData.PageWidth, MyEnvelopeData.PageHeight);
                //Size pageSize = new Size(printDialog.PrintableAreaWidth, printDialog.PrintableAreaHeight);  //todo cemu ovo kada se nigde ne koristi
                IDocumentPaginatorSource idpSource = document;
                printDialog.PrintDocument(idpSource.DocumentPaginator, "Secure Envelope Print");
            }
            else
            {
                throw new Exception("No printer with the name " + MyEnvelopeData.PrinterName + " found.");
            }
        }

      
    }


}
