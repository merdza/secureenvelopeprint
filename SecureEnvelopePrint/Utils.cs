﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureEnvelopePrintNS
{
    static class Utils
    {
        public static void WriteToEventLog(string eventLogMessage, EventLogEntryType type)
        {
            using (EventLog eventLog = new EventLog())
            {
                if (!EventLog.SourceExists("SecureEnvelopePrint"))
                {
                    EventLog.CreateEventSource("SecureEnvelopePrint", "SecureEnvelopePrint Log");
                }
                else
                {
                    eventLog.Source = "SecureEnvelopePrint";  //ime servisa
                    eventLog.WriteEntry(eventLogMessage, type, 10110);  //id servisa, menjaj samo poslednji broj
                }
            }
        }

        //ex 15.05.2018. ili bez tacke na kraju
        public static string GetShortSerbianDate(DateTime dt, bool dotAfterYear)
        {
            string ret = AddLeadingZeros(dt.Day, 2) + @"." + AddLeadingZeros(dt.Month, 2) + @"." + AddLeadingZeros(dt.Year, 2);
            if (dotAfterYear)
            {
                ret += @".";
            }
            return ret;
        }

        public static string AddLeadingZeros(int i, int totalNumbers)
        {
            try
            {
                int mul = 10;
                string lead = "";
                for (int j = totalNumbers; j > 1; j--)
                {
                    if (i < mul)
                    {
                        for (int k = 0; k < j - 1; k++)
                        {
                            lead += "0";
                        }
                        return lead + i.ToString();
                    }
                    mul *= 10;
                }
                return i.ToString();
            }
            catch (Exception)
            {
                //ma ovo nema sanse da se desi
                //try catch je samo da ne pukne program
                return "0";
            }
        }
    }
}
