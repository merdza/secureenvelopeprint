﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureEnvelopePrintNS
{
    public class EnvelopeDataP12Password : EnvelopeData
    {
        public string Password { get; set; }

        public EnvelopeDataP12Password(string printerName, EnvelopeTypeEnum envelopeType, string cn, string password, string legalEntityName, string name, string lastName, string street, string streetNo, string postNo, string city, string pak, int pageWidth, int pageHeight)
            : base(printerName, envelopeType, cn, legalEntityName, name, lastName, street, streetNo, postNo, city, pak, pageWidth, pageHeight)
        {
            Password = password;
            
        }
    }
}
