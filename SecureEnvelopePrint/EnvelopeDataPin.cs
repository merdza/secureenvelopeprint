﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureEnvelopePrintNS
{
    class EnvelopeDataPin : EnvelopeDataPinAndPuk
    {
        //puk je prazan string
        public EnvelopeDataPin(string printerName, EnvelopeTypeEnum envelopeType, string pin, string cn, string legalEntityName, string name, string lastName, string street, string streetNo, string postNo, string city, string pak, int pageWidth, int pageHeight)
            : base(printerName, envelopeType, pin, string.Empty, cn, legalEntityName, name, lastName, street, streetNo, postNo, city, pak, pageWidth, pageHeight)
        {
        }
    }
}
